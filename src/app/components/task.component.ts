import {Component, Input, OnInit} from '@angular/core';
import {Task} from '../models/task';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-task',
  template: `
    <app-card color="purple">
      <div class="flex justify-between mb-2">
        <p class="text-lg font-bold"> {{task.title}}</p>
        <button (click)="deleteTask()" class="bg-red-200 text-red-900 p-2 rounded-lg shadow hover:bg-red-300  ml-2">
          <app-svg-remove></app-svg-remove>
        </button>
      </div>
      <p class="text-sm">{{task.description}}</p>
    </app-card>
  `,
})
export class TaskComponent implements OnInit {
  @Input() task: Task;
  db: any;
  constructor(afs: AngularFirestore) {
    this.db = afs.collection('tasks');
  }

  ngOnInit(): void {
  }

  deleteTask(): void {
    this.db.doc(this.task.id).delete();
  }

}
