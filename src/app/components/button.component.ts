import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  template: `
    <button type="button" class="px-4 py-2 w-full h-full" (click)="buttonClick.emit()">
      {{text}}
    </button>
  `,
})
export class ButtonComponent implements OnInit {

  @Input() text: string;
  @Input() color: string;
  @Output() buttonClick = new EventEmitter<number>();

  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `rounded-md shadow-sm mx-1 inline-flex
    justify-center w-full rounded-md border border-transparent
    text-base leading-6 font-medium text-white shadow-sm focus:outline-none
    transition ease-in-out duration-150 sm:text-sm sm:leading-5
    bg-${this.color}-600 hover:bg-${this.color}-500 focus:border-${this.color}-700 focus:shadow-outline-${this.color}`;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
