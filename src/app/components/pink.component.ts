import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-pink',
  template: `
    <app-card color="pink">
      <div class="flex justify-between">
        <p> Pink component </p>
        <button (click)="deleteComponent()"
                class="bg-red-200 text-red-900 p-2 rounded-lg shadow hover:bg-red-300 focus:outline-none">
          <app-svg-remove></app-svg-remove>
        </button>
      </div>
    </app-card>
  `,
})
export class PinkComponent implements OnInit {
  @Input() component: any;
  @Output() delete = new EventEmitter<number>();
  db: any;

  constructor(afs: AngularFirestore) {
    this.db = afs.collection('pinkComponents');
  }

  ngOnInit(): void {
  }

  deleteComponent(): void {
    this.db.doc(this.component.id).delete();
  }
}
