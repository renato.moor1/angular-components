import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IGenericComponent} from '../models/IGenericComponent';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-grey',
  template: `
    <app-card color="gray" full="true">
      <div class="flex justify-between mb-3">
        <p class="self-center">Grey component</p>
        <button (click)="addChild()" class="bg-green-200 p-2 rounded-lg shadow hover:bg-green-300 text-green-500">
          <app-svg-add></app-svg-add>
        </button>
      </div>

      <div *ngFor="let component of components | async">
        <app-pink (delete)="removeChildById($event)" [component]="component"></app-pink>
      </div>
    </app-card>
  `,
})

export class GreyComponent implements OnInit {
  components: Observable<IGenericComponent[]>;
  db: any;

  constructor(firestore: AngularFirestore) {
    this.db = firestore.collection<IGenericComponent>('pinkComponents');
    this.components = this.db.valueChanges();
  }

  ngOnInit(): void {}

  addChild(): void {
    const id = Math.random().toString(36).slice(2);
    this.db.doc(id).set({id});
  }

  removeChildById(id: number): void {

  }
}
