import {Component, Input, OnInit} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {IGenericComponent} from '../models/IGenericComponent';

@Component({
  selector: 'app-orange',
  template: `
    <app-card [classes]="orangeComponent.isOpen ? 'h-48': ''" color="orange">
      <div class="flex justify-between">
        <p>
          Orange component {{ orangeComponent.id }}
        </p>
        <button (click)="toggle()" class="text-orange-500 outline-none p-2 focus:outline-none bg-orange-200 rounded-lg shadow">
          <svg *ngIf="!orangeComponent.isOpen" class="h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path
              d="M12 24c-.066 0-.13-.013-.192-.039-.061-.025-.116-.062-.163-.109l-4.999-4.999c-.195-.195-.195-.512 0-.707.095-.094.221-.146.354-.146s.259.052.354.146l4.146 4.146V1.707L7.354 5.853C7.259 5.948 7.134 6 7 6s-.259-.052-.353-.147c-.195-.195-.195-.512 0-.707l5-5c.046-.046.101-.083.162-.108C11.87.013 11.934 0 12 0s.13.013.191.038c.062.026.116.062.163.108l5 5c.195.195.195.512 0 .707-.095.095-.22.147-.354.147s-.259-.052-.354-.146L12.5 1.707v20.586l4.146-4.146c.095-.095.221-.147.354-.147s.259.052.354.146c.195.195.195.512 0 .707l-5 5c-.046.046-.101.083-.163.109-.061.025-.125.038-.191.038z"/>
          </svg>

          <svg *ngIf="orangeComponent.isOpen" class="h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
            <path
              d="M.5 23.999c-.276 0-.5-.224-.5-.5s.224-.5.5-.5H11v-7.293l-2.146 2.146c-.094.094-.22.146-.354.146s-.259-.052-.354-.146c-.195-.195-.195-.512 0-.707l3-3c.046-.046.101-.083.163-.109.06-.025.125-.038.191-.038s.13.013.192.038c.061.025.116.062.163.109l2.999 2.999c.195.195.195.512 0 .707-.094.094-.22.146-.354.146s-.259-.052-.354-.146L12 15.706v7.293h11.5c.276 0 .5.224.5.5s-.224.5-.5.5H.5zM11.5 10.999c-.066 0-.13-.013-.191-.038-.062-.026-.116-.062-.162-.108l-3-3c-.195-.195-.195-.512 0-.707.094-.095.219-.147.353-.147s.259.052.354.146L11 9.292V.999H.5c-.276 0-.5-.224-.5-.5s.224-.5.5-.5h23c.276 0 .5.224.5.5s-.224.5-.5.5H12v8.293l2.146-2.146c.094-.094.22-.146.354-.146s.259.052.354.146c.195.195.195.512 0 .707l-3 3c-.046.046-.101.083-.162.108-.062.025-.126.038-.192.038z"/>
          </svg>
        </button>
      </div>
    </app-card>
  `,
})
export class OrangeComponent implements OnInit {
  @Input() orangeComponent: IGenericComponent;
  db: any;

  constructor(afs: AngularFirestore) {
    this.db = afs.collection('orangeComponents');
  }

  ngOnInit(): void {
  }

  deleteTask(): void {
    this.db.doc(this.orangeComponent.id).delete();
  }

  toggle(): void {
    const isOpen = this.orangeComponent.isOpen = !this.orangeComponent.isOpen;
    this.db.doc(this.orangeComponent.id).update({
      id: this.orangeComponent.id,
      isOpen,
    });
  }
}
