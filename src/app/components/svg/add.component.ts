import {Component} from '@angular/core';

@Component({
  selector: 'app-svg-add',
  template: `
    <svg class="h-4 w-4 fill-current"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
      <path
        d="M12 24c-.414 0-.75-.336-.75-.75v-10.5H.75c-.414 0-.75-.336-.75-.75s.336-.75.75-.75h10.5V.75c0-.414.336-.75.75-.75s.75.336.75.75v10.5h10.5c.414 0 .75.336.75.75s-.336.75-.75.75h-10.5v10.5c0 .414-.336.75-.75.75z"/>
    </svg>
  `,
})
export class AddComponent {

}
