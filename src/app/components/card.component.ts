import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div
      class="{{ classes}} border shadow rounded p-5 text-center m-1
      bg-{{color}}-100 border-{{color}}-400 text-{{color}}-700
      hover:border-{{color}}-300 hover:text-{{color}}-800 hover:shadow-lg {{ full && 'h-full' }}">
      <ng-content></ng-content>
    </div>
  `,
})
export class CardComponent implements OnInit {
  @Input() color: string;
  @Input() full: boolean;
  @Input() classes: string;

  constructor() {
    this.full = false;
  }

  ngOnInit(): void {
  }
}
