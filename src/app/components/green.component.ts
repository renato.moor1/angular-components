import {Component, HostBinding, OnInit} from '@angular/core';

@Component({
  selector: 'app-green',
  template: `
    <app-card color="green" class="w-full mt-5">
      <p>Green Component</p>
    </app-card>
  `,
})
export class GreenComponent implements OnInit {

  constructor() {
  }
  @HostBinding('class')
  // tslint:disable-next-line:typedef
  get themeClass() {
    return `mb-2`;
  }

  ngOnInit(): void {
  }

}
