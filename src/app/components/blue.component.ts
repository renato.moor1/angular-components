import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IGenericComponent} from '../models/IGenericComponent';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-blue',
  template: `
    <app-card color="blue" full="true">
      <p>Blue component</p>
      <div *ngFor="let component of orangeComponents | async">
        <app-orange [orangeComponent]="component"></app-orange>
      </div>
    </app-card>
  `,
})
export class BlueComponent implements OnInit {
  @Input() orangeComponents: Observable<IGenericComponent[]>;
  db: any;

  constructor(firestore: AngularFirestore) {
    this.db = firestore.collection<IGenericComponent>('orangeComponents');
    this.orangeComponents = this.db.valueChanges();
  }

  ngOnInit(): void {

  }
}
