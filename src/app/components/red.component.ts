import {Component, OnInit} from '@angular/core';
import {Task} from '../models/task';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-red',
  template: `
    <app-card color="red" full="true">
      <div class="flex justify-between mb-2">
        <app-modal (addTask)="addTask($event)" (close)="closeModal()" [open]="open"></app-modal>
        <p> Red component </p>
        <button (click)="openModal()" class="bg-green-200 p-2 rounded-lg shadow hover:bg-green-300 text-green-500">
          <app-svg-add></app-svg-add>
        </button>
      </div>
      <div class="flex flex-wrap">
        <div *ngFor="let task of tasks | async">
          <app-task [task]="task"></app-task>
        </div>
      </div>
    </app-card>
  `,
})
export class RedComponent implements OnInit {
  open: boolean;
  tasks: Observable<Task[]>;
  db: any;

  constructor(firestore: AngularFirestore) {
    this.db = firestore.collection<Task>('tasks');
    this.tasks = this.db.valueChanges();
  }

  ngOnInit(): void {
  }

  openModal(): void {
    this.open = true;
  }

  addTask(task: Task): void {
    const id = Math.random().toString(36).slice(2);
    this.db.doc(id).set({id, ...task});
    this.closeModal();
  }

  closeModal(): void {
    this.open = false;
  }
}
