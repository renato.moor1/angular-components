import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal',
  template: `
    <div *ngIf="open" class="fixed z-10 inset-0 overflow-y-auto">
      <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
          <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;
        <div
          class="inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl
           transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6"
          role="dialog" aria-modal="true" aria-labelledby="modal-headline">
          <p class="text-2xl text-gray-600 font-bold text-center">Add task</p>
          <label class="block text-sm font-medium leading-5 text-gray-700">Title</label>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <input [(ngModel)]="title" (keydown.escape)="closeModal()" (keydown.enter)="add()"
                   class="form-input block w-full sm:text-sm sm:leading-5"
                   placeholder="Task title">
          </div>
          <label class="block text-sm font-medium leading-5 text-gray-700">Description</label>
          <div class="mt-1 relative rounded-md shadow-sm text-gray-700">
            <textarea [(ngModel)]="description" (keydown.escape)="closeModal()" (keydown.enter)="add()"
                      class="form-input block w-full sm:text-sm sm:leading-5"
                      placeholder="Task description"></textarea>
          </div>
          <div class="mt-5 sm:mt-6">
        <span class="flex w-full">
          <app-button color="red" text="Cancel" (buttonClick)="closeModal()"></app-button>
          <app-button color="green" text="Create" (buttonClick)="add()"></app-button>
        </span>
          </div>
        </div>
      </div>
    </div>
  `,
})

export class ModalComponent implements OnInit {
  title: string;
  description: string;
  @Input() open: boolean;

  @Output() close = new EventEmitter<number>();
  @Output() addTask = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  closeModal(): void {
    this.close.emit();
  }

  add(): void {
    const taskModel = {
      title: this.title,
      description: this.description
    };
    this.addTask.emit(taskModel);
    this.title = '';
    this.description = '';
  }
}
