import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GreenComponent } from './components/green.component';
import { GreyComponent } from './components/grey.component';
import { PinkComponent } from './components/pink.component';
import { BlueComponent } from './components/blue.component';
import { OrangeComponent } from './components/orange.component';
import { RedComponent } from './components/red.component';
import { CardComponent } from './components/card.component';
import {ModalComponent} from './components/modal.component';
import {FormsModule} from '@angular/forms';
import {TaskComponent} from './components/task.component';
import {AddCommand} from '@angular/cli/commands/add-impl';
import {AddComponent} from './components/svg/add.component';
import {ButtonComponent} from './components/button.component';
import {RemoveComponent} from './components/svg/remove.component';

@NgModule({
  declarations: [
    AppComponent,
    GreenComponent,
    GreyComponent,
    PinkComponent,
    BlueComponent,
    OrangeComponent,
    RedComponent,
    CardComponent,
    ModalComponent,
    TaskComponent,
    AddComponent,
    ButtonComponent,
    RemoveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
