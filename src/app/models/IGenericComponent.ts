export interface IGenericComponent {
  id: string;
  isOpen: boolean;
}
